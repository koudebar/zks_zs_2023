# ZKS_ZS_2023

This is repository for front-end testing of Magento using Selenium

Created by Barbora Koudelkova and Matyas Koval'

This project will focus on the second variant of the assignment, specifically:
Variant 2 - front-end tests
Any web-based application is feasible to use - we will be utilizing a Magento sandbox eshop (https://magento.softwaretestingboard.com/)

![img_1.png](img_1.png)

![img.png](img.png)
