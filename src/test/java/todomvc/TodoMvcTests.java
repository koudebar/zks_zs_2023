package todomvc;

import net.serenitybdd.annotations.Managed;
import net.serenitybdd.junit5.SerenityJUnit5Extension;
import net.serenitybdd.screenplay.actions.Enter;
import org.apache.groovy.parser.antlr4.internal.atnmanager.LexerAtnManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.converter.ConvertWith;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import todomvc.converters.*;
import todomvc.pageObjects.ApplicationHomePage;
import todomvc.tasks.*;
import org.openqa.selenium.chrome.ChromeOptions;
import org.junit.jupiter.api.AfterEach;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.WebElement;

import net.serenitybdd.junit5.SerenityJUnit5Extension;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;
import java.time.Duration;


import static net.serenitybdd.screenplay.GivenWhenThen.*;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.*;
import static todomvc.questions.TodoItemsListQuestion.theDisplayedItems;
import static todomvc.questions.FinishedOrderIndication.theOrderFinishedSuccesfully;
import static todomvc.questions.WishlistExpandIndicator.theItemAddedToWishlist;
import static todomvc.questions.CompareContentIndicator.theItemsAddedToCompare;

@ExtendWith(SerenityJUnit5Extension.class)
public class TodoMvcTests {

    Actor james = Actor.named("James");
    Actor zdenek = Actor.named("Zdenek");
    Actor marek = Actor.named("Marek");
    Actor kristyna = Actor.named("Kristyna");

    String categoryName = "women";

        //@Managed
    private WebDriver theBrowser;


    @BeforeEach
    public void before() throws MalformedURLException{
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--headless");
        options.addArguments("--disable-gpu");
        options.addArguments("--no-sandbox");
        options.addArguments("--disable-dev-shm-usage");
        options.addArguments("window-size=2560,1600");
        theBrowser = new RemoteWebDriver(new URL("http://selenium:4444/wd/hub"), options);
        //theBrowser = new ChromeDriver(options);
        james.can(BrowseTheWeb.with(theBrowser));
        zdenek.can(BrowseTheWeb.with(theBrowser));
        marek.can(BrowseTheWeb.with(theBrowser));
        kristyna.can(BrowseTheWeb.with(theBrowser));
    }


    @AfterEach
    public void tearDown() {
        if (theBrowser != null) {
            theBrowser.quit();
        }
    }


    /*@Test
    public void should_be_able_to_add_the_first_todo_item() {
        String todoName = "Learn Selenium";

        givenThat(james).wasAbleTo(StartWith.anEmptyTodoList());
        when(james).attemptsTo(AddATodoItem.called(todoName));
        then(james).should(seeThat(theDisplayedItems(), hasItem(todoName)));
    }*/

    @Test
    public void should_be_able_to_see_the_category(){
        String desiredObject = "Breathe-Easy Tank";
        givenThat(zdenek).wasAbleTo(StartWith.visitingTheWebsite());
        when(zdenek).attemptsTo(SelectCategory.named(categoryName));
        WebDriverWait waitForCategory = new WebDriverWait(theBrowser, Duration.ofSeconds(5));
        waitForCategory.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div.products-grid ol.product-items")));
        then(zdenek).should(seeThat(theDisplayedItems(), hasItem(containsString(desiredObject))));
    }

    @Test
    public void should_be_able_to_buy() {
        String desiredObject = "Breathe-Easy Tank";
        desiredObject = desiredObject.toLowerCase();
        desiredObject = desiredObject.replaceAll("\\s", "-");
        givenThat(zdenek).wasAbleTo(StartWith.visitingTheWebsite());
        when(zdenek).attemptsTo(BuyAnItem.in(desiredObject, categoryName));
        WebDriverWait waitForCheckout = new WebDriverWait(theBrowser, Duration.ofSeconds(5));
        waitForCheckout.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#maincontent div.checkout-success")));
        then(zdenek).should(seeThat(theOrderFinishedSuccesfully(), equalTo(true) ));

    }

    @Test
    public void should_be_able_to_favourite() {
        String desiredObject = "Argus All-Weather Tank";
        String URLobject = desiredObject.toLowerCase();
        URLobject = URLobject.replaceAll("\\s", "-");
        givenThat(marek).wasAbleTo(StartWith.visitingTheWebsite());
        givenThat(marek).wasAbleTo(ContinueWith.login());
        WebDriverWait waitForRet = new WebDriverWait(theBrowser, Duration.ofSeconds(5));
        waitForRet.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div.product-item-info a[href='https://magento.softwaretestingboard.com/"+URLobject+".html']")));
        when(marek).attemptsTo(FavouriteAnItem.in(URLobject, categoryName));
        WebDriverWait waitForWishlist = new WebDriverWait(theBrowser, Duration.ofSeconds(5));
        waitForWishlist.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".message-success.success.message")));
        then(marek).should(seeThat(theItemAddedToWishlist(desiredObject), equalTo(true) ));
    }

    @Test
    public void should_be_able_to_compare() {
        givenThat(kristyna).wasAbleTo(StartWith.visitingTheWebsite());
        String[] objsToCompare = {"Radiant Tee", "Breathe-Easy Tank", "Selene Yoga Hoodie", "Deirdre Relaxed-Fit Capri"};
        for (String s : objsToCompare) {
            //System.out.println(objsToCompare[i]);
            String URLobject = s.toLowerCase();
            URLobject = URLobject.replaceAll("\\s", "-");
            when(kristyna).attemptsTo(CompareAnItem.in(URLobject, categoryName));
            theBrowser.get("https://magento.softwaretestingboard.com/");
        }
        when(kristyna).attemptsTo(VisitThe.Comparation());
        WebDriverWait waitForCompare = new WebDriverWait(theBrowser, Duration.ofSeconds(5));
        waitForCompare.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#product-comparison")));
        then(kristyna).should(seeThat(theItemsAddedToCompare(objsToCompare), equalTo(true)));
    }

    @ParameterizedTest
    @CsvFileSource(
            resources = "/Magentotest-output2Way.csv",
            delimiter = ',',
            numLinesToSkip = 1)
    public void yourTest(@ConvertWith(EmailConverter.class) String email,
                         @ConvertWith(NameConverter.class) String name,
                         @ConvertWith(SurameConverter.class) String surname,
                         @ConvertWith(AddressConverter.class) String address,
                         @ConvertWith(CityConverter.class) String city,
                         @ConvertWith(CountryConverter.class) String countryAddr,
                         @ConvertWith(ZipConverter.class) String zip,
                         @ConvertWith(StateConverter.class) String countryState,
                         @ConvertWith(PhoneConverter.class) String phone,
                         String contentPrice,
                         String validity) throws Exception{
        try {

            // get to the checkout form
            theBrowser.get("https://magento.softwaretestingboard.com/");
            theBrowser.findElement(By.cssSelector("div.product-item-info a[href='https://magento.softwaretestingboard.com/radiant-tee.html']")).click();
            WebDriverWait waitS = new WebDriverWait(theBrowser, Duration.ofSeconds(5)); // Adjust timeout as needed
            waitS.until(ExpectedConditions.elementToBeClickable(By.cssSelector("div.swatch-opt div[option-label='S']")));
            theBrowser.findElement(By.cssSelector("div.swatch-opt div[option-label='S']")).click();
            theBrowser.findElement(By.cssSelector("div.swatch-opt div[option-label='Purple']")).click();
            theBrowser.findElement(By.cssSelector(".action.tocart.primary")).click();
            WebDriverWait waitForMessage = new WebDriverWait(theBrowser, Duration.ofSeconds(10));
            waitForMessage.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".message-success.success.message")));
            theBrowser.get("https://magento.softwaretestingboard.com/checkout");
            /*WebDriverWait waitForCheckoutForm = new WebDriverWait(theBrowser, Duration.ofSeconds(5));
            waitForCheckoutForm.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#checkout-step-shipping")));*/

            // Locate form input fields and submit button
            WebDriverWait waitForCheckoutEmail = new WebDriverWait(theBrowser, Duration.ofSeconds(10));
            waitForCheckoutEmail.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#checkout-step-shipping #customer-email")));
            WebElement emailInput = theBrowser.findElement(By.cssSelector("#checkout-step-shipping #customer-email"));

            WebDriverWait waitForCheckoutName = new WebDriverWait(theBrowser, Duration.ofSeconds(10));
            waitForCheckoutName.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#checkout-step-shipping #shipping-new-address-form div.field[name='shippingAddress.firstname'] input.input-text")));
            WebElement nameInput = theBrowser.findElement(By.cssSelector("#checkout-step-shipping #shipping-new-address-form div.field[name='shippingAddress.firstname'] input.input-text"));

            WebDriverWait waitForCheckoutSurname = new WebDriverWait(theBrowser, Duration.ofSeconds(10));
            waitForCheckoutSurname.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#checkout-step-shipping #shipping-new-address-form div.field[name='shippingAddress.lastname'] input.input-text")));
            WebElement surnameInput = theBrowser.findElement(By.cssSelector("#checkout-step-shipping #shipping-new-address-form div.field[name='shippingAddress.lastname'] input.input-text"));
            WebElement street0 = theBrowser.findElement(By.cssSelector("#checkout-step-shipping #shipping-new-address-form div.field[name='shippingAddress.street.0'] input.input-text"));
            WebElement street1 = theBrowser.findElement(By.cssSelector("#checkout-step-shipping #shipping-new-address-form div.field[name='shippingAddress.street.1'] input.input-text"));
            WebElement street2 = theBrowser.findElement(By.cssSelector("#checkout-step-shipping #shipping-new-address-form div.field[name='shippingAddress.street.2'] input.input-text"));
            WebElement cityInput = theBrowser.findElement(By.cssSelector("#checkout-step-shipping #shipping-new-address-form div.field[name='shippingAddress.city'] input.input-text"));
            WebElement zipInput = theBrowser.findElement(By.cssSelector("#checkout-step-shipping #shipping-new-address-form div.field[name='shippingAddress.postcode'] input.input-text"));
            WebElement countrySelector = theBrowser.findElement(By.cssSelector("#checkout-step-shipping div.field[name='shippingAddress.country_id'] div.control"));
            WebElement stateSelector = theBrowser.findElement(By.cssSelector("#checkout-step-shipping div.field[name='shippingAddress.region_id'] div.control"));
            WebElement phoneInput = theBrowser.findElement(By.cssSelector("#checkout-step-shipping #shipping-new-address-form div.field[name='shippingAddress.telephone'] input.input-text"));

            /*String countryAddr = "United States";
            String countryState = "Alabama";
    */
            // Add more elements as needed

            // Fill in the form with valid data
            emailInput.sendKeys(email);
            nameInput.sendKeys(name);
            surnameInput.sendKeys(surname);

           if(address.equals("Morávkova Ulice 42/B")){
                String [] address2 = address.split("\\s+");
                street0.sendKeys(address2[0]+address2[1]);
                System.out.println(address2[0]+address2[1]);
                street1.sendKeys(address2[2]);
                System.out.println(address2[2]);
            } else if(address.equals("Morávkova Ulice 42 apartmá B")){
                String [] address3 = address.split("\\s+");
                System.out.println(address);
                street0.sendKeys(address3[0]+address3[1]);
                System.out.println(address3[0]+address3[1]);
                street1.sendKeys(address3[2]);
                System.out.println(address3[2]);
                street2.sendKeys(address3[3]);
                System.out.println(address3[3]);
            } else {
               street0.sendKeys(address);
               System.out.println(address);
           }

            cityInput.sendKeys(city);
            zipInput.sendKeys(zip);

            WebDriverWait wait = new WebDriverWait(theBrowser, Duration.ofSeconds(5)); // Adjust timeout as needed
            wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("div#checkout-loader.loading-mask")));
            countrySelector.click();
            WebElement country = theBrowser.findElement(By.cssSelector("#checkout-step-shipping div.field[name='shippingAddress.country_id'] option[data-title='"+countryAddr+"']"));
            WebDriverWait waitForCheckoutCountrySelect = new WebDriverWait(theBrowser, Duration.ofSeconds(5));
            waitForCheckoutCountrySelect.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#checkout-step-shipping div.field[name='shippingAddress.country_id'] div.control")));
            country.click();
            if (countryAddr.equals("United States")){
                stateSelector.click();
                WebElement state = theBrowser.findElement(By.cssSelector("#checkout-step-shipping div.field[name='shippingAddress.region_id'] option[data-title='"+countryState+"']"));
                state.click();
            }
            phoneInput.sendKeys(phone);

            // Have to choose a delivery method and try to send data
            theBrowser.findElement(By.cssSelector("#opc-shipping_method #label_carrier_flatrate_flatrate")).click();
            theBrowser.findElement(By.cssSelector("#shipping-method-buttons-container div.primary")).click();
            /*WebDriverWait waitForPlaceOrderButt = new WebDriverWait(theBrowser, Duration.ofSeconds(10));
            waitForPlaceOrderButt.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#checkout-payment-method-load div.actions-toolbar div.primary")));
            theBrowser.findElement(By.cssSelector("#checkout-payment-method-load div.actions-toolbar div.primary")).click();*/

            theBrowser.quit();

            if (validity.equalsIgnoreCase("valid")) {
                // Assert that the test is successful based on the expected validity
                // Add assertions here to validate the success of the test
                assertTrue(true, "Test is valid");
            } else {
                // If the test was expected to be invalid but completed without exceptions
                // Assert that the test fails, as it was expected to be invalid
                fail("Test was expected to be invalid but completed without exceptions");
            }
        } catch (Exception e) {
            // If an exception occurs during the test execution
            if (validity.equalsIgnoreCase("valid")) {
                // If the test was expected to be valid but encountered an exception
                // Assert that the test fails, as it was expected to be valid
                fail("Test was expected to be valid but encountered an exception: " + e.getMessage());
            } else {
                // If the test was expected to be invalid and encountered an exception
                // Assert that the test is successful, as it was expected to be invalid
                assertTrue(true, "Test is invalid due to exception: " + e.getMessage());
            }
        }
    }

}
