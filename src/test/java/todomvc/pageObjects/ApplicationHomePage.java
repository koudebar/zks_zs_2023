package todomvc.pageObjects;

import net.serenitybdd.annotations.DefaultUrl;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;

@DefaultUrl("https://magento.softwaretestingboard.com/")
public class ApplicationHomePage extends PageObject {

    public static Target category(String category){
        return Target
                .the(category + " Category Button")
                //.locatedBy("li a.sf-with-ul[title='" + category + "']");}
                .locatedBy("li.ui-menu-item a.ui-corner-all[href='https://magento.softwaretestingboard.com/" + category + ".html']");}
    public static final Target LIST_ITEMS = Target
            .the("product list")
            .locatedBy("div.products-grid ol.product-items");

    public static final Target LOGIN_LINK = Target
            .the("login link button")
            .locatedBy("div.panel.header a[href='https://magento.softwaretestingboard.com/customer/account/login/referer/aHR0cHM6Ly9tYWdlbnRvLnNvZnR3YXJldGVzdGluZ2JvYXJkLmNvbS8%2C/']");
    public static final Target LOGIN_BUTTON = Target
            .the("login  button")
            .locatedBy("div.actions-toolbar #send2");

    public static final Target CART_SHOP = Target
            .the("Cart Indicator")
            .locatedBy(".shopping_cart .ajax_cart_quantity");

    public static final Target SUCCESS_PAGE = Target
            .the("Success page")
            .locatedBy("#maincontent div.checkout-success");
    public static Target OUTFIT(String outfit){
        return Target
                .the(outfit)
                .locatedBy("div.product-item-info a[href='https://magento.softwaretestingboard.com/"+outfit+".html']");

    }
    public static final Target SIZE_S = Target
            .the("Size S button")
            .locatedBy("div.swatch-opt div[option-label='S']");

    public static final Target COLOUR_PURP = Target
            .the("Coulour purple button")
            .locatedBy("div.swatch-opt div[option-label='Purple']");

    public static final Target ADD_TO_CART_BUTTON = Target
            .the("Add to cart button")
            .locatedBy(".action.tocart.primary");

    public static final Target ADD_TO_WISHLIST_BUTTON = Target
            .the("Add to wishlist button")
            .locatedBy("div.product-addto-links a[data-action='add-to-wishlist']");

    public static final Target ADD_TO_COMPARE_BUTTON = Target
            .the("Add to compare button")
            .locatedBy("div.product-addto-links a.action.tocompare");

    public static final Target CART_BUTTON = Target
            .the("Cart button")
            .locatedBy(".action.showcart");

    public static final Target COMPARE_BUTTON = Target
            .the("Compare button")
            .locatedBy("div.header.content a.action.compare");

    public static final Target PRODUCT_COMPARISON_TABLE = Target
            .the("Product comparison table")
            .locatedBy("#product-comparison");

    public static final Target HOME_BUTTON = Target
            .the("Home button")
            .locatedBy("div.page-wrapper a[aria-label='store logo']");

    public static final Target CHECKOUT_BUTTON = Target
            .the("Checkout button")
            .locatedBy(".action.primary.checkout");

    public static final Target ADDED_MESSAGE = Target
            .the("Cart added message")
            .locatedBy(".message-success.success.message");

    public static final Target CHECKOUT_FORM = Target
            .the("Checkout form")
            .locatedBy("#checkout-step-shipping");

    public static final Target EMAIL_INPUT = Target
            .the("Email input")
            .locatedBy("#checkout-step-shipping #customer-email");
    public static final Target EMAIL_MAREK_INPUT = Target
            .the("Marks Email input")
            .locatedBy("div.block.block-customer-login #email");

    public static final Target PASS_MAREK_INPUT = Target
            .the("Marks password input")
            .locatedBy("div.block.block-customer-login #pass");

    public static final Target NAME_INPUT = Target
            .the("Name input")
            .locatedBy("#checkout-step-shipping #shipping-new-address-form div.field[name='shippingAddress.firstname'] input.input-text");

    public static final Target SURNAME_INPUT = Target
            .the("Surname input")
            .locatedBy("#checkout-step-shipping #shipping-new-address-form div.field[name='shippingAddress.lastname'] input.input-text");

    public static final Target ZIP_INPUT = Target
            .the("ZIP input")
            .locatedBy("#checkout-step-shipping #shipping-new-address-form div.field[name='shippingAddress.postcode'] input.input-text");
    public static final Target CITY_INPUT = Target
            .the("City input")
            .locatedBy("#checkout-step-shipping #shipping-new-address-form div.field[name='shippingAddress.city'] input.input-text");

    public static final Target PHONE_INPUT = Target
            .the("Phone input")
            .locatedBy("#checkout-step-shipping #shipping-new-address-form div.field[name='shippingAddress.telephone'] input.input-text");
    public static final Target ADDRESS_INPUT1 = Target
            .the("Adress line 1 input")
            .locatedBy("#checkout-step-shipping #shipping-new-address-form div.field[name='shippingAddress.street.0'] input.input-text");
    public static final Target ADDRESS_INPUT2 = Target
            .the("Adress line 2 input")
            .locatedBy("#checkout-step-shipping #shipping-new-address-form div.field[name='shippingAddress.street.1'] input.input-text");
    public static final Target ADDRESS_INPUT3 = Target
            .the("Adress line 3 input")
            .locatedBy("#checkout-step-shipping #shipping-new-address-form div.field[name='shippingAddress.street.2'] input.input-text");

    public static final Target BEST_WAY_BUTTON = Target
            .the("Best way delivery button form")
            .locatedBy("#opc-shipping_method #label_carrier_bestway_tablerate");
    public static final Target NEXT_BUTTON = Target
            .the("Next buttonin checkout")
            .locatedBy("#shipping-method-buttons-container div.primary");

    public static final Target STATE_DROP_DOWN = Target
            .the("State selector drop down menu checkout")
            .locatedBy("#checkout-step-shipping div.field[name='shippingAddress.region_id'] div.control");

    public static final Target ALABAMA = Target
            .the("State selector drop down menu Alabama option")
            .locatedBy("#checkout-step-shipping div.field[name='shippingAddress.region_id'] option[data-title='Alabama']");

    public static final Target PLACE_ORDER_BUTTON = Target
            .the("Place order button at the end of checkout")
            .locatedBy("#checkout-payment-method-load div.actions-toolbar div.primary");
}