package todomvc.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

import java.util.Collection;
import java.util.List;

import static todomvc.pageObjects.ApplicationHomePage.LIST_ITEMS;

/*public class TodoItemsListQuestion {

    public static Question<Collection<String>> theDisplayedItems() {
        return Text.ofEach(LIST_ITEMS).describedAs("the items displayed");
    }
}*/
public class TodoItemsListQuestion implements Question<List<String>> {

    @Override
    public List<String> answeredBy(Actor actor) {
        return (List<String>) Text.ofEach(LIST_ITEMS).answeredBy(actor);
    }

    public static Question<List<String>> theDisplayedItems() {
        return new TodoItemsListQuestion();
    }
}