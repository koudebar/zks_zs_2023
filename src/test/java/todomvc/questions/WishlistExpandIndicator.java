package todomvc.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;
import todomvc.pageObjects.ApplicationHomePage;

public class WishlistExpandIndicator implements Question<Boolean> {

    String desiredObject;

    public WishlistExpandIndicator(String desiredObject) {

        this.desiredObject = desiredObject;
    }

    @Override
    public Boolean answeredBy(Actor actor) {

        String response = Text.of(ApplicationHomePage.ADDED_MESSAGE).answeredBy(actor);
        System.out.println("Response from the page: " + response);
        return response.contains(desiredObject);
    }

    public static Question<Boolean> theItemAddedToWishlist(String desiredObject) {
        return new WishlistExpandIndicator(desiredObject);
    }
}

