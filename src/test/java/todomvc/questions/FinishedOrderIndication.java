package todomvc.questions;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.questions.Text;
import todomvc.pageObjects.ApplicationHomePage;

public class FinishedOrderIndication implements Question<Boolean> {

    @Override
    public Boolean answeredBy(Actor actor) {

        String response = Text.of(ApplicationHomePage.SUCCESS_PAGE).answeredBy(actor);
        System.out.println("Response from the page: " + response);
        return response.contains("Your order # is: ");
    }

    public static Question<Boolean> theOrderFinishedSuccesfully() {
        return new FinishedOrderIndication();
    }
}
