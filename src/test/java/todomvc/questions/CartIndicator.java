package todomvc.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;
import todomvc.pageObjects.ApplicationHomePage;

import java.util.concurrent.atomic.AtomicInteger;

public class CartIndicator implements Question<Boolean> {

    @Override
    public Boolean answeredBy(Actor actor) {

        String cartItemCount = Text.of(ApplicationHomePage.CART_SHOP).answeredBy(actor);
        return Integer.parseInt(cartItemCount) > 0;
    }

    public static Question<Boolean> theCartIsNotEmpty() {
        return new CartIndicator();
    }
}