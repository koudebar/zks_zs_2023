package todomvc.questions;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;
import todomvc.pageObjects.ApplicationHomePage;

public class CompareContentIndicator implements Question<Boolean> {

    String[] desiredObjects;

    public CompareContentIndicator(String[] desiredObjects) {

        this.desiredObjects = desiredObjects;
    }

    @Override
    public Boolean answeredBy(Actor actor) {

        String table = Text.of(ApplicationHomePage.PRODUCT_COMPARISON_TABLE).answeredBy(actor);
        System.out.println("Response from the page: " + table);

        boolean ret = true;
        for (String temp : desiredObjects) {
            if (!table.contains(temp)) {
                ret = false;
                break;
            }
        }
        return ret;
    }

    public static Question<Boolean> theItemsAddedToCompare(String[] desiredObjects) {
        return new CompareContentIndicator(desiredObjects);
    }
}
