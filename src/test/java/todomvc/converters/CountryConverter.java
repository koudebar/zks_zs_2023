package todomvc.converters;

import org.junit.jupiter.params.converter.ArgumentConversionException;
import org.junit.jupiter.params.converter.SimpleArgumentConverter;

public class CountryConverter extends SimpleArgumentConverter {
    @Override
    protected Object convert(Object o, Class<?> aClass)
            throws ArgumentConversionException {

        switch (o.toString())
        {
            case "reqState": return "United States";
            case "notReqState": return "Czechia";
            case "null": return null;
            default:
                throw new ArgumentConversionException(o.toString());
        }
    }
}
