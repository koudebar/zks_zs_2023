package todomvc.converters;

import org.junit.jupiter.params.converter.ArgumentConversionException;
import org.junit.jupiter.params.converter.SimpleArgumentConverter;

public class StateConverter extends SimpleArgumentConverter {
    @Override
    protected Object convert(Object o, Class<?> aClass)
            throws ArgumentConversionException {

        switch (o.toString())
        {
            case "valid": return "Alabama";
            case "null": return null;
            default:
                throw new ArgumentConversionException(o.toString());
        }
    }
}
