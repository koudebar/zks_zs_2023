package todomvc.converters;

import org.junit.jupiter.params.converter.ArgumentConversionException;
import org.junit.jupiter.params.converter.SimpleArgumentConverter;

public class PhoneConverter extends SimpleArgumentConverter {
    @Override
    protected Object convert(Object o, Class<?> aClass)
            throws ArgumentConversionException {

        switch (o.toString())
        {
            case "invalidWithoutPrefix": return "0";
            case "validWithoutPrefix": return "603603603";
            case "invalidWithPrefix": return "+4200";
            case "validWithPrefix": return "+420603603603";
            case "null": return null;
            default:
                throw new ArgumentConversionException(o.toString());
        }
    }
}
