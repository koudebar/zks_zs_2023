package todomvc.converters;

import org.junit.jupiter.params.converter.ArgumentConversionException;
import org.junit.jupiter.params.converter.SimpleArgumentConverter;

public class ZipConverter extends SimpleArgumentConverter {
    @Override
    protected Object convert(Object o, Class<?> aClass)
            throws ArgumentConversionException {

        switch (o.toString())
        {
            case "valid": return "35004";
            case "invalid": return "35    notZip@#$?<!004";
            case "null": return null;
            default:
                throw new ArgumentConversionException(o.toString());
        }
    }
}
