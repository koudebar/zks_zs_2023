package todomvc.converters;

import org.junit.jupiter.params.converter.ArgumentConversionException;
import org.junit.jupiter.params.converter.SimpleArgumentConverter;

public class AddressConverter extends SimpleArgumentConverter {
    @Override
    protected Object convert(Object o, Class<?> aClass)
            throws ArgumentConversionException {

        switch (o.toString())
        {
            case "invalid": return "123456789101112131415161718192021";
            case "valid": return "Morávkova Ulice 42";
            case "splitIn2": return "Morávkova Ulice 42/B";
            case "splitIn3": return "Morávkova Ulice 42 apartmá B";
            case "null": return null;
            default:
                throw new ArgumentConversionException(o.toString());
        }
    }
}
