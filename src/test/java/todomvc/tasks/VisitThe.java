package todomvc.tasks;
import net.serenitybdd.annotations.Step;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import todomvc.pageObjects.ApplicationHomePage;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class VisitThe implements Task{

    public static VisitThe Comparation() {
        return instrumented(VisitThe.class);
    }

    @Override
    @Step("{0} starts with an empty todo list")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(ApplicationHomePage.COMPARE_BUTTON)
        );
    }
}
