package todomvc.tasks;
import net.serenitybdd.annotations.Step;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import todomvc.pageObjects.ApplicationHomePage;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;
import net.serenitybdd.screenplay.waits.WaitUntil;

public class CompareAnItem implements Task {

    String outfit;
    String category;
    public CompareAnItem(String outfit, String category) {
        this.category = category;
        this.outfit = outfit;
    }

    public static CompareAnItem in(String outfit, String category) {
        return new CompareAnItem(outfit, category);
    }


    @Override
    @Step("{0} adds an item to the cart from the category: {1}")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                //HoverOverTarget.over(ApplicationHomePage.OUTFIT(outfit)),
                //WaitUntil.the(ApplicationHomePage.ADD_TO_WISHLIST_BUTTON, isVisible()).forNoMoreThan(5).seconds(),
                Click.on(ApplicationHomePage.category(category)),
                Click.on(ApplicationHomePage.OUTFIT(outfit)),
                Click.on(ApplicationHomePage.ADD_TO_COMPARE_BUTTON)
                //WaitUntil.the(ApplicationHomePage.HOME_BUTTON, isVisible()).forNoMoreThan(30).seconds(),
                //Click.on(ApplicationHomePage.HOME_BUTTON)

        );
    }
}
