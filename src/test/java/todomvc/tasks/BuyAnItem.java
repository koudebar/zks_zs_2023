package todomvc.tasks;

import net.serenitybdd.annotations.Step;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.waits.WaitUntil;
import net.serenitybdd.screenplay.actions.Enter;
import todomvc.pageObjects.ApplicationHomePage;

import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class BuyAnItem implements Task {

    String category;
    String outfit;

    public BuyAnItem(String outfit, String category) {

        this.category = category;
        this.outfit = outfit;
    }
    public static BuyAnItem in(String outfit, String category) {
        return new BuyAnItem(outfit, category);
    }


    @Override
    @Step("{0} adds an item to the cart from the category: {1}")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(

                Click.on(ApplicationHomePage.category(category)),
                Click.on(ApplicationHomePage.OUTFIT(outfit)),
                WaitUntil.the(ApplicationHomePage.SIZE_S, isVisible()).forNoMoreThan(5).seconds(),
                Click.on(ApplicationHomePage.SIZE_S),
                WaitUntil.the(ApplicationHomePage.COLOUR_PURP, isVisible()).forNoMoreThan(5).seconds(),
                Click.on(ApplicationHomePage.COLOUR_PURP),
                //HoverOverTarget.over(ApplicationHomePage.OUTFIT),
                Click.on(ApplicationHomePage.ADD_TO_CART_BUTTON),
                WaitUntil.the(ApplicationHomePage.ADDED_MESSAGE, isVisible()).forNoMoreThan(5).seconds(),
                Click.on(ApplicationHomePage.CART_BUTTON),
                Click.on(ApplicationHomePage.CHECKOUT_BUTTON),
                //WaitUntil.the(ApplicationHomePage.CHECKOUT_FORM, isVisible()).forNoMoreThan(5).seconds(),
                WaitUntil.the(ApplicationHomePage.EMAIL_INPUT, isVisible()).forNoMoreThan(5).seconds(),
                Enter.theValue("example@example.com").into(ApplicationHomePage.EMAIL_INPUT),
                WaitUntil.the(ApplicationHomePage.NAME_INPUT, isVisible()).forNoMoreThan(5).seconds(),
                Enter.theValue("Zdenda").into(ApplicationHomePage.NAME_INPUT),
                WaitUntil.the(ApplicationHomePage.SURNAME_INPUT, isVisible()).forNoMoreThan(5).seconds(),
                Enter.theValue("Davidovec").into(ApplicationHomePage.SURNAME_INPUT),
                WaitUntil.the(ApplicationHomePage.CITY_INPUT, isVisible()).forNoMoreThan(5).seconds(),
                Enter.theValue("Alabama").into(ApplicationHomePage.CITY_INPUT),
                WaitUntil.the(ApplicationHomePage.ZIP_INPUT, isVisible()).forNoMoreThan(5).seconds(),
                Enter.theValue("35004").into(ApplicationHomePage.ZIP_INPUT),
                WaitUntil.the(ApplicationHomePage.PHONE_INPUT, isVisible()).forNoMoreThan(5).seconds(),
                Enter.theValue("8007472140").into(ApplicationHomePage.PHONE_INPUT),
                Enter.theValue("Zdenduv domov 420").into(ApplicationHomePage.ADDRESS_INPUT1),
                Enter.theValue("").into(ApplicationHomePage.ADDRESS_INPUT2),
                Enter.theValue("").into(ApplicationHomePage.ADDRESS_INPUT3),
                Click.on(ApplicationHomePage.STATE_DROP_DOWN),
                Click.on(ApplicationHomePage.ALABAMA),
                Click.on(ApplicationHomePage.BEST_WAY_BUTTON),
                Click.on(ApplicationHomePage.NEXT_BUTTON),
                WaitUntil.the(ApplicationHomePage.PLACE_ORDER_BUTTON, isVisible()).forNoMoreThan(5).seconds(),
                Click.on(ApplicationHomePage.PLACE_ORDER_BUTTON)
                );
    }
}