package todomvc.tasks;

import net.serenitybdd.annotations.Step;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import todomvc.pageObjects.ApplicationHomePage;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isClickable;
import net.serenitybdd.screenplay.waits.WaitUntil;

public class FavouriteAnItem implements Task {

    String category;
    String outfit;

    public FavouriteAnItem(String outfit, String category) {

        this.category = category;
        this.outfit = outfit;
    }
    public static FavouriteAnItem in(String outfit, String category) {
        return new FavouriteAnItem(outfit, category);
    }


    @Override
    @Step("{0} adds an item to the cart from the category: {1}")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                //HoverOverTarget.over(ApplicationHomePage.OUTFIT(outfit)),
                WaitUntil.the(ApplicationHomePage.OUTFIT(outfit), isClickable()).forNoMoreThan(5).seconds(),
                Click.on(ApplicationHomePage.OUTFIT(outfit)),
                Click.on(ApplicationHomePage.ADD_TO_WISHLIST_BUTTON)

                );
    }
}
