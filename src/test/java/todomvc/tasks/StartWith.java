package todomvc.tasks;

import net.serenitybdd.annotations.Step;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Open;
import todomvc.pageObjects.ApplicationHomePage;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class StartWith implements Task {

    ApplicationHomePage applicationHomePage;

    public static StartWith visitingTheWebsite() {
        return instrumented(StartWith.class);
    }

    @Override
    @Step("{0} starts with an empty todo list")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
            Open.browserOn().the(applicationHomePage)
        );
    }
}