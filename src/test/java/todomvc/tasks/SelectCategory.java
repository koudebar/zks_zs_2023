package todomvc.tasks;

import net.serenitybdd.annotations.Step;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Open;
import todomvc.pageObjects.ApplicationHomePage;

public class SelectCategory implements Task {

    String category;
    ApplicationHomePage applicationHomePage;

    public SelectCategory(String category) {
        this.category = category;
    }

   public static SelectCategory named(String category) {
        return new SelectCategory(category);
    }

    @Override
    @Step("{0} selects the category: {1}")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(ApplicationHomePage.category(category))
        );
    }
}