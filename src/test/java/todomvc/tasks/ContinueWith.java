package todomvc.tasks;

import net.serenitybdd.annotations.Step;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import todomvc.pageObjects.ApplicationHomePage;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class ContinueWith implements Task {

    public static ContinueWith login() {
        return instrumented(ContinueWith.class);
    }

    @Override
    @Step("{0} starts with an empty todo list")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(ApplicationHomePage.LOGIN_LINK),
                Enter.theValue("marek.test@test.com").into(ApplicationHomePage.EMAIL_MAREK_INPUT),
                Enter.theValue("Test1234").into(ApplicationHomePage.PASS_MAREK_INPUT),
                Click.on(ApplicationHomePage.LOGIN_BUTTON)
        );
    }
}
